import {buatTable} from "./tabel.js";

import { columns, data} from "./library.js";

const table = new buatTable( {columns, data});
const app = document.getElementById("app");
table.render(app);
