const listNews = document.getElementById('listNews');
const searchBar = document.getElementById('searchinput');
const characterList = document.getElementById('listNews'); 
let news = [];
// let display = [];

searchBar.addEventListener('keyup', (e) => {
  const cari = e.target.value.toLowerCase();
  const cariFiltered = news.filter((pencarian) => {
    return (pencarian.title.toLowerCase().includes(cari) || 
            pencarian.description.toLowerCase().includes(cari)
            );
  });
  displayCharacters(cariFiltered);
  
});

const loadNews = () => {
  var url = 'https://newsapi.org/v2/top-headlines?' +
          'country=id&' +
          'apiKey=1bf14bcb4a644371ad86f7c92cf4ba0c';
  var req = new Request(url);
  fetch(req)
    .then(response => {
      return response.json();
    }).then(responseJson => {
      news = responseJson.articles;
      displayCharacters(news);
    }
    )
  };

  const displayCharacters = (characters) => {
    const htmlString = characters
        .map((item) => {
            return `
            <div class="col-4 mb-5">
            <div class="card" style="width: 18rem;">
              <img src= "${item.urlToImage}" class="card-img-top" alt="...">
              <div class="card-body" id="listBerita">
                <h5 class="card-title">${item.title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${item.author} "-" ${item.publishedAt}</h6>
                <p class="card-text">${item.description}</p>
              </div>
            </div>
            </div>
        `;
        })
        .join('');
    listNews.innerHTML = htmlString;
};
loadNews();